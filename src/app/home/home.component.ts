import { Component, OnInit } from '@angular/core';
import { ProductServieService } from '../service/product-servie.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  listeProduit;
  constructor(private productService:ProductServieService) { }

  ngOnInit() {
  }
  afficherproduit (){

    this.productService.getAllProduct().subscribe(res=>{
      console.log(res)
      this.listeProduit = res ;

    })
  }
}
