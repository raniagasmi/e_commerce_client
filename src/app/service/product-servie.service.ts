import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
@Injectable({
  providedIn: 'root'
})
export class ProductServieService {
  baseUrl = environment.baseUrl
  constructor(private http: HttpClient) { }

  getAllProduct() {
    return this.http.get(this.baseUrl + '/produit/afficher')
  }




}
